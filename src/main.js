import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

import { routes } from './routes';
import './directives/transform';

Vue.use(VueRouter);

const router = new VueRouter({
  routes : routes,
  mode:'history'
});

Vue.use(VueResource);
Vue.http.options.root = 'http://www.lefisc.com.br/gnre/apiGNRE'

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})